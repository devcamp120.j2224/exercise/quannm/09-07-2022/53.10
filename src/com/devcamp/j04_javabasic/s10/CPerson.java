package com.devcamp.j04_javabasic.s10;
import java.util.ArrayList;
public class CPerson extends CAnimal {
	/**
	 * 
	 */
	public CPerson() {
		super();
	}
	/**
	 * khởi tạo Cperson với đủ các tham số
	 * @param id
	 * @param age
	 * @param firstName
	 * @param lastName
	 * @param pets
	 */
	public CPerson(int id, int age, String firstName, String lastName, ArrayList<CPet> pets) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.pets = pets;
	}
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList <CPet> pets;
	@Override
	public void animalSound() {
		System.out.println("person speaking.");

	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the pets
	 */
	public ArrayList<CPet> getPets() {
		return pets;
	}
	/**
	 * @param pets the pets to set
	 */
	public void setPets(ArrayList<CPet> pets1) {
		this.pets = pets1;
	}
	@Override
	public String toString() {
		return "CPerson {\"id\":" + this.id + ", age=" + age + ", firstName=" 
		+ firstName + ", lastName=" + lastName + ", pets=" + this.pets + "}";
	}
}
