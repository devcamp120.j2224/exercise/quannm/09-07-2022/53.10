package com.devcamp.j04_javabasic.s10;

public interface ISumable {
	String  getSum();
}
