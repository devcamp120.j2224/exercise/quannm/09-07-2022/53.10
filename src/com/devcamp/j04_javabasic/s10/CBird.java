package com.devcamp.j04_javabasic.s10;
public class CBird extends CPet implements IFlyable {
	@Override
	public void fly() {
		System.out.println("Bird flying");
	}
}
